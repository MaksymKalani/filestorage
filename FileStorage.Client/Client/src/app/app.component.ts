import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';
import {ViewEncapsulation} from '@angular/core';
import { Roles } from './constants/Roles';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AppComponent {
  title = 'Filery';
  isAuth = false;
  isAdmin = false;
  public login: string;

  constructor(private authService: AuthService, private router: Router){
    this.authService.currentUser.subscribe( user => {
      if (user){
        this.isAuth = true;
        this.login = user.username;
        if(user.roles.includes(Roles.Admin)){
          this.isAdmin = true;
        }
      }
      else{
        this.isAuth = false;
        this.login = '';
      }
    });

  }
  ngOnInit(){
   const currentUser =  this.authService.getCurrentUserValue();
   this.isAuth = currentUser == null ? false : true;
   this.login = currentUser == null ? '' : currentUser.username;
  }

  logout(){
    this.authService.logout();
    this.router.navigate(['/signin']);
  }
 }


