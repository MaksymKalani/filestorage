import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Roles } from 'src/app/constants/Roles';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninComponent implements OnInit {

  loginForm: FormGroup;
  loginError: string;
  constructor(private authService: AuthService, private router: Router) {

   }
  ngOnInit() {
    this.loginError = '';
    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  login(){
    this.authService.signin(this.loginForm.value).subscribe(
      (user) => {

        if (user.roles.includes(Roles.Admin) ){
            this.router.navigate(['/files']);
        }
        else{
          this.router.navigate(['/files']);
              }
      },
      (exc) => {
        console.log(exc);
        this.loginError = Object.values(exc.errors)[0] + '';
      }
    );


  }


}
