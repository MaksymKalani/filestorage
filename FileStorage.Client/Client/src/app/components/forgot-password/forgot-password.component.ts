import { AuthService } from './../../services/auth.service';
import { UserServiceService } from './../../services/user-service.service';
import { ForgotPassword } from './../../interfaces/ForgotPassword';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { FileService } from 'src/app/services/file-service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
  forgotForm: FormGroup;
  forgotError: any;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.forgotForm = new FormGroup({
      email: new FormControl(''),
    });
  }
  send(){
    const forgot = this.forgotForm.value;
    this.authService.forgot(forgot).subscribe(
      (error) => {
        this.forgotError = Object.values(error)[0];
      }
    );

    if (this.forgotError !== ''){
      this.router.navigate(['/restore-password']);
    }

  }

}
