import { Component, OnInit } from '@angular/core';
import { UserItem } from 'src/app/interfaces/UserItem';
import { UserServiceService } from 'src/app/services/user-service.service';
import { BrowserModule } from '@angular/platform-browser';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {

  constructor(private userservice: UserServiceService) { }
  users: UserItem[];

  ngOnInit() {
    this.updateUsersList();
    console.log('users');
  }

  updateUsersList(): void{
    this.userservice.getUsers().subscribe((data) => {
      this.users = data;
    });
  }


}
