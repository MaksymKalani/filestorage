import { FormControl, FormGroup } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-restore-password',
  templateUrl: './restore-password.component.html',
  styleUrls: ['./restore-password.component.scss']
})
export class RestorePasswordComponent implements OnInit {
  restoreForm: FormGroup;
  restoreError: string;
  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.restoreForm = new FormGroup({
      token: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
      confirmPassword: new FormControl('')
    });
  }

  restore(){
    const restore = this.restoreForm.value;
    this.authService.reset(restore).subscribe(
      () => {
        this.router.navigate(['/signin']);
      },
      (exc) => {
            this.restoreError = Object.values(exc)[0] + '';
            console.log(this.restoreError);
      }
    );
  }

}
