import { AuthService } from './../../services/auth.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  constructor(private formBuiuder: FormBuilder, private authService: AuthService, private router: Router) { }
  checkStudForm: FormGroup;
  signupForm: FormGroup;
  registerError: any;

  ngOnInit() {

    this.signupForm = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      email: new FormControl(''),
      password: new FormControl(''),
      confirmPassword: new FormControl('')
    });

    this.registerError = '';
  }

  register(){
    const userRegister = this.signupForm.value;

    this.authService.signup(userRegister).subscribe(
      ()=>{
        this.router.navigate(['/signin']);
      },
      (exc) =>{

            this.registerError = Object.values(exc)[0] + '';
            console.log(this.registerError);
      }
    );
  }


}
