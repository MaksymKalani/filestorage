import { File } from './../../interfaces/File';
import { FileService } from './../../services/file-service';
import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import {MatButtonModule} from '@angular/material/button';
import { catchError, map } from 'rxjs/operators';
import { HttpErrorResponse, HttpEventType } from '@angular/common/http';
import { of } from 'rxjs';
import { ElementRef } from '@angular/core';
import { ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';

@Component({
  selector: 'app-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.css']
})
export class FilesComponent implements OnInit {

  @ViewChild('fileUpload', {static: false}) fileUpload: ElementRef; filesUpload  = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  constructor(private fileservice: FileService) { }

  files: File[] = [];
  path: string;

  ngOnInit(): void {
    this.updateFilesList();
  }

  updateFilesList(): void{
    this.fileservice.getFiles().subscribe((data) => {
      this.files = data;
      this.path = 'https://localhost:44363/api/';
    });
  }

  uploadFile(fileUpload) {
    const formData = new FormData();
    formData.append('file', fileUpload.data);
    fileUpload.inProgress = true;
    this.fileservice.uploadFile(formData).pipe(
      map(event => {
        switch (event.type) {
          case HttpEventType.UploadProgress:
            fileUpload.progress = Math.round(event.loaded * 100 / event.total);
            break;
          case HttpEventType.Response:
            return event;
        }
      }),
      catchError((error: HttpErrorResponse) => {
        fileUpload.inProgress = false;
        return of(`${fileUpload.data.name} upload failed.`);
      })).subscribe((event: any) => {
        if (typeof (event) === 'object') {
          console.log(event.body);
        }
      });
    this.updateFilesList();
  }

  private uploadFiles() {
    this.fileUpload.nativeElement.value = '';
    this.filesUpload.forEach(file => {
      this.uploadFile(file);
    });
    this.updateFilesList();
  }


  onClick() {
    const fileUpload = this.fileUpload.nativeElement;
    fileUpload.onchange = () => {
      for (let index = 0; index < fileUpload.files.length; index++)
      {
        const file = fileUpload.files[index];
        this.filesUpload.push({ data: file, inProgress: false, progress: 0});
      }
      this.uploadFiles();
    };
    fileUpload.click();
  }

  delete(id){
    this.fileservice.deleteFile(id);
    this.updateFilesList();
  }

  onCheck(){

  }
}
