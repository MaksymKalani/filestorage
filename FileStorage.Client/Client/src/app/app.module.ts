import { SideMenuComponent } from './components/side-menu/side-menu.component';
import { RolesComponent } from './components/roles/roles.component';
import { RestorePasswordComponent } from './components/restore-password/restore-password.component';
import { LandingComponent } from './components/landing/landing.component';
import { ForgotPasswordComponent } from './components/forgot-password/forgot-password.component';
import { ForgotPassword } from './interfaces/ForgotPassword';
import { UsersComponent } from 'src/app/components/users/users.component';
import { RouterModule } from '@angular/router';
import {ClipboardModule} from '@angular/cdk/clipboard';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgxFilesizeModule} from 'ngx-filesize';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FilesComponent } from './components/files/files.component';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatButtonModule} from '@angular/material/button';
import { SigninComponent } from './components/signin/signin.component';
import { SignupComponent } from './components/signup/signup.component';
import { AppRoutingModule } from './routing/app-routing/app-routing.module';
import {MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { JwtInterseptor } from './helpers/jwt-interceptor';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatIconModule} from '@angular/material/icon';
import {MatSnackBar} from '@angular/material/snack-bar';
import {MatMenuModule} from '@angular/material/menu';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
@NgModule({
  declarations: [
    AppComponent,
    FilesComponent,
      SigninComponent,
      SignupComponent,
      UsersComponent,
      ForgotPasswordComponent,
      LandingComponent,
      RestorePasswordComponent,
      RolesComponent
   ],
  imports: [
    BrowserModule,
    HttpClientModule,
    NgxFilesizeModule,
    BrowserAnimationsModule,
    MatCardModule,
    ReactiveFormsModule,
    MatToolbarModule,
    MatFormFieldModule,
    MatInputModule,
    AppRoutingModule,
    MatButtonModule,
    ReactiveFormsModule,
    FormsModule,
    ClipboardModule,
    MatIconModule,
    MatProgressBarModule,
    MatIconModule,
    MatMenuModule,
    MatButtonToggleModule,
    MatPaginatorModule,
    MatSlideToggleModule
  ],
  providers: [{
    provide: HTTP_INTERCEPTORS, useClass: JwtInterseptor, multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
