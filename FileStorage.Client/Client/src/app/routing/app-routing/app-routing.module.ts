import { RestorePasswordComponent } from './../../components/restore-password/restore-password.component';
import { ForgotPasswordComponent } from './../../components/forgot-password/forgot-password.component';
import { RolesComponent } from './../../components/roles/roles.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { FilesComponent } from 'src/app/components/files/files.component';
import { SigninComponent } from 'src/app/components/signin/signin.component';
import { SignupComponent } from 'src/app/components/signup/signup.component';
import { AuthGuard } from 'src/app/helpers/auth-guard';
import { LandingComponent } from 'src/app/components/landing/landing.component';
import { UsersComponent } from 'src/app/components/users/users.component';


const routes: Routes = [
  { path: 'signin', component: SigninComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'files', component: FilesComponent },
  { path: 'landing', component: LandingComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'restore-password', component: RestorePasswordComponent },
  { path: 'users', component: UsersComponent, canActivate: [AuthGuard],
    data: {
      role: ['Admin']
    }
  },
  { path: 'roles', component: RolesComponent, canActivate: [AuthGuard],
    data: {
      role: ['Admin']
    }
  },
  { path: '', redirectTo: '/landing', pathMatch: 'full' }
];

@NgModule({
  imports: [
      RouterModule.forRoot(routes)
  ],
  exports: [
      RouterModule
  ],
  declarations: []
})
export class AppRoutingModule { }
