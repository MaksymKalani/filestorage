import { AuthService } from './../services/auth.service';
import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
@Injectable({providedIn: 'root'})
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router){

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot){
    const currentUser  = this.authService.getCurrentUserValue();
    if (currentUser){
      return  currentUser.roles.includes(route.data.role);
    }

    this.router.navigate(['/landing']);
    return false;

  }
}
