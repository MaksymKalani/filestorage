export interface File {
    id: number;
    name: string;
    extension: string;
    size: number;
    hash: string;
    userId: string;
}
