export interface UserItem {
  id: string;
  email: string;
  firstName: string;
  lastName: string;
  fileIds: number[];
  roles: string[];
}
