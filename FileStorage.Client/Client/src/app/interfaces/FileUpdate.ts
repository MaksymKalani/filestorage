export interface FileUpdate {
  id: number;
  name: string;
  extension: string;
}
