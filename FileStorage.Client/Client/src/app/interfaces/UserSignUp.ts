export interface UserSignUp {
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  confirmpassword: string;
}
