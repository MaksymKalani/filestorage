/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { RoleSeriveService } from './role-serive.service';

describe('Service: RoleSerive', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RoleSeriveService]
    });
  });

  it('should ...', inject([RoleSeriveService], (service: RoleSeriveService) => {
    expect(service).toBeTruthy();
  }));
});
