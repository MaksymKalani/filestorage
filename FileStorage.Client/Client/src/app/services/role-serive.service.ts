import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RoleSeriveService {

getUrl = 'https://localhost:44363/api/Admin/Roles';
postUrl = 'https://localhost:44363/api/Admin/Roles';
postUserUrl = 'https://localhost:44363/api/Admin/User/Roles';
deleteUrl = 'https://localhost:44363/api/Admin/Roles';
deleteUserUrl = 'https://localhost:44363/api/Admin/User/Role';

constructor(private http: HttpClient) { }

getRoles(): Observable<string[]>{
  return this.http.get<string[]>(this.getUrl).pipe(
    map((data: string[]) => {
      return data;
    }),
    catchError(error => {
      console.log(error);
      return throwError(error);
    })
  );
}

postRole(role: string){
  return this.http.post<any>(this.postUrl, role, {
    reportProgress: true,
    observe: 'events'
  });
}

deleteRole(role: string){
  return this.http.delete<any>(this.deleteUrl + role)
  .subscribe({
    error: error => {
        console.error('There was an error!', error);
    }
});
}

postUserRole(role: string, userEmail: string){
  return this.http.post<any>(this.postUserUrl, {userEmail, role}, {
    reportProgress: true,
    observe: 'events'
  });
}

deleteUserRole(role: string, userEmail: string){
  return this.http.delete<any>(this.deleteUrl + {role, userEmail})
  .subscribe({
    error: error => {
        console.error('There was an error!', error);
    }
});
}
}
