import { ResetPassword } from './../interfaces/ResetPassword';
import { ForgotPassword } from './../interfaces/ForgotPassword';
import { FormControl } from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { UserSignIn } from './../interfaces/UserSignIn';
import { Injectable } from '@angular/core';
import { UserSignUp } from './../interfaces/UserSignUp';
import { User } from './../interfaces/User';
import {map, catchError} from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  forgotUrl = 'https://localhost:44363/api/Auth/Forgot';
  resetUrl = 'https://localhost:44363/api/Auth/Reset';

  private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  constructor(private http: HttpClient) {
    this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
    this.currentUser = this.currentUserSubject.asObservable();
   }

   public getCurrentUserValue(): User{
    return this.currentUserSubject.value;
  }

  signup(userSignUp: UserSignUp){
    return this.http.post('https://localhost:44363/api/Auth/signup', userSignUp);
  }

  signin(userSignIn: UserSignIn){
    return this.http.post('https://localhost:44363/api/Auth/signin', userSignIn).pipe(
      map((user: User) => {
        localStorage.setItem('currentUser', JSON.stringify(user));
        this.currentUserSubject.next(user);
        return user;

      }
      )
    );
  }

  logout(){
    localStorage.removeItem('currentUser');
    this.currentUserSubject.next(null);
  }

  forgot(forgot: ForgotPassword){
    return this.http.post<any>(this.forgotUrl, forgot);
  }

  reset(reset: ResetPassword){
    return this.http.post<any>(this.resetUrl, reset);
  }
}
