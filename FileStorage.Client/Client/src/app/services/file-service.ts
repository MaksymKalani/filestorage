import { FileUpdate } from './../interfaces/FileUpdate';
import { HttpClient, HttpEvent, HttpErrorResponse, HttpEventType } from '@angular/common/http';
import {File} from './../interfaces/File';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import {map, catchError} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FileService {

getUrl = 'https://localhost:44363/api/File';
postUrl = 'https://localhost:44363/api/upload';
deleteUrl = 'https://localhost:44363/api/File/';
putUrl = 'https://localhost:44363/api/File/';
shareUrl = 'https://localhost:44363/api/File/Share';
restrictUrl = 'https://localhost:44363/api/File/Restrict';

constructor(private http: HttpClient) { }

getFiles(): Observable<File[]>{
  return this.http.get<File[]>(this.getUrl).pipe(
    map((data: File[]) => {
      return data;
    }),
    catchError(error => {
      console.log(error);
      return throwError(error);
    })
  );
}

getFile(id: number): Observable<File>{
  return this.http.get<File>(this.getUrl + id).pipe(
    map((data: File) => {
      return data;
    }),
    catchError(error => {
      console.log(error);
      return throwError(error);
    })
  );
}


uploadFile(formData){
  return this.http.post<any>(this.postUrl, formData, {
    reportProgress: true,
    observe: 'events'
  });
}
deleteFile(id: number){
  return this.http.delete<any>(this.deleteUrl + id)
  .subscribe({
    error: error => {
        console.error('There was an error!', error);
    }
});
}
updateFile(id: number, file: FileUpdate){
  return this.http.put<any>(this.putUrl + id, file)
  .subscribe({
    error: error => {
        console.error('There was an error!', error);
    }
});
}

shareFile(id: number){
  return this.http.post<any>(this.shareUrl, id)
  .subscribe({
    error: error => {
        console.error('There was an error!', error);
    }
});
}

}
