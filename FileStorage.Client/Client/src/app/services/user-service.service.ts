import { UserUpdate } from './../interfaces/UserUpdate';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { UserItem } from '../interfaces/UserItem';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

getUrl = 'https://localhost:44363/api/User';
putUrl = 'https://localhost:44363/api/User';
deleteUrl = 'https://localhost:44363/api/User/User/';
constructor(private http: HttpClient) { }

getUsers(): Observable<UserItem[]>{
  return this.http.get<UserItem[]>(this.getUrl).pipe(
    map((data: UserItem[]) => {
      return data;
    }),
    catchError(error => {
      console.log(error);
      return throwError(error);
    })
  );
}

updateUser(user: UserUpdate){
  return this.http.put<any>(this.putUrl, user, {
    reportProgress: true,
    observe: 'events'
  });
}

deleteUser(id: string){
  return this.http.delete<any>(this.deleteUrl + id)
  .subscribe({
    error: error => {
        console.error('There was an error!', error);
    }
});
}
}
