﻿using FileStorage.BLL.DTO;
using FileStorage.BLL.Exceptions;
using FileStorage.BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FileStorage.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public UserController(IUserService userService, IHttpContextAccessor httpContextAccessor)
        {
            this._userService = userService;
            this._httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Authorize(Roles = "Admin")]
        public async Task<ActionResult<IEnumerable<UserDto>>> GetAllUsersAsync()
        {
            var users = await _userService.GetAllAsync();
            return Ok(users);
        }

        /// <summary>
        /// Updates user info
        /// </summary>
        /// <param name="userUpdateDto"></param>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult<FileDto>> UpdateUser(UserUpdateDto userUpdateDto)
        {
            Guid userId;
            try
            {
                userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            }
            catch
            {
                userId = default;
            }
            if (userId != userUpdateDto.Id && !User.IsInRole("Admin"))
                return StatusCode(403, "You have no access to this user");

            try
            {
                await _userService.UpdateUser(userUpdateDto);
            }
            catch (FileStorageException ex)
            {
                switch (ex.ResponseCode)
                {
                    case 404:
                        return NotFound();
                    case 500:
                        return Problem(ex.Message, null, 500);
                }
            }

            return Ok();
        }

        /// <summary>
        /// Removes user
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("User/{id}")]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteUser(Guid id)
        {
            try
            {
                await _userService.DeleteUser(id);
            }
            catch (ArgumentException ex)
            {
                return NotFound(ex.Message);
            }
            return NoContent();
        }
    }
}
