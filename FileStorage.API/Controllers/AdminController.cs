﻿using FileStorage.BLL.Exceptions;
using FileStorage.BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace FileStorage.API.Controllers
{
    [Authorize(Roles = "Admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IUserService _userService;
        public AdminController(IUserService userService)
        {
            this._userService = userService;
        }

        /// <summary>
        /// returns all roles
        /// </summary>
        /// <returns></returns>
        [HttpGet("Roles")]
        public async Task<IActionResult> GetRoles()
        {
            var roles = await _userService.GetRoles();
            return Ok(roles);
        }

        [HttpDelete("Roles")]
        public async Task<IActionResult> DeleteRole(string roleName)
        {
            try
            {
                await _userService.DeleteRole(roleName);
            }
            catch(FileStorageException ex)
            {
                switch (ex.ResponseCode)
                {
                    case 404:
                            return NotFound(ex.Message);
                    case 500:
                            return BadRequest(ex.Message);
                }
            }
            
            return Ok();
        }


        /// <summary>
        /// Creates new role
        /// </summary>
        /// <param name="roleName"></param>
        /// <returns></returns>
        [HttpPost("Roles")]
        public async Task<IActionResult> CreateRole(string roleName)
        {
            try
            {
                await _userService.CreateRole(roleName);
            }
            catch (FileStorageException ex)
            {
                switch (ex.ResponseCode)
                {
                    case 400:
                        return BadRequest("Role name should be provided.");
                    case 500:
                        return BadRequest(ex.Message);
                }
            }
            return Ok();
        }

        /// <summary>
        /// Assigns role to user
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        [HttpPost("User/Role")]
        public async Task<IActionResult> AddUserToRole(string userEmail, string roleName)
        {
            try
            {
                await _userService.AddUserToRole(userEmail, roleName);
            }
            catch (FileStorageException ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

        /// <summary>
        /// Removes role from user
        /// </summary>
        /// <param name="userEmail"></param>
        /// <param name="roleName"></param>
        /// <returns></returns>
        [HttpDelete("User/Role")]
        public async Task<IActionResult> RemoveUserFromRole(string userEmail, string roleName)
        {
            try
            {
                await _userService.RemoveUserFromRole(userEmail, roleName);
            }
            catch (FileStorageException ex)
            {
                return BadRequest(ex.Message);
            }
            return Ok();
        }

    }
}
