﻿using FileStorage.BLL.DTO;
using FileStorage.BLL.Exceptions;
using FileStorage.BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using System;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FileStorage.API.Controllers
{
    [Route("api/")]
    [ApiController]
    public class LoadController : ControllerBase
    {
        private readonly IFileService _fileService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public LoadController(IFileService fileService,
            IHttpContextAccessor httpContextAccessor)
        {
            this._fileService = fileService;
            this._httpContextAccessor = httpContextAccessor;
        }

        [HttpGet("{id}")]
        public async Task<IActionResult> Download(int id)
        {
            Guid userId;
            try
            {
                userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            } 
            catch
            {
                userId = default;
            }

            var target = await _fileService.GetFileByIdAsync(id);
            if (target == null)
                return NotFound("There is no file with such id");
            if (target.IsRestricted && userId != target.UserId && !User.IsInRole("Admin"))
                return StatusCode(403, "You have no access to this file");

            MemoryStream stream;
            try
            {
                stream = await _fileService.GetFileStreamById(id);
            }
            catch (FileStorageException ex)
            {
                return BadRequest(ex.Message);
            }

            FileDto file;
            try
            {
                file = await _fileService.GetFileByIdAsync(id);
            }
            catch (FileNotFoundException ex)
            {
                return BadRequest(ex.Message);
            }

            if (stream == null || file == null)
                return NotFound();

            var FileName = $"{file.Name}.{file.Extension}";
            string contentType;
            new FileExtensionContentTypeProvider().TryGetContentType(FileName, out contentType);
            return new FileStreamResult(stream, contentType ?? "application/octet-stream")
            {
                FileDownloadName = FileName
            };
        }

        [Authorize]
        [HttpPost("Upload")]
        public async Task<IActionResult> Upload(IFormFile file)
        {
            if (file == null)
                return BadRequest();
            if (file.Length > 100000000)
                return BadRequest("File size could not be bigger than 100Mb");

            using (Stream s = file.OpenReadStream())
            {
                using (BinaryReader br = new BinaryReader(s))
                {
                    byte[] bytes = br.ReadBytes((Int32)s.Length);

                    FileDto newFile = new FileDto
                    {
                        Name = Path.GetFileNameWithoutExtension(file.FileName),
                        Extension = Path.GetExtension(file.FileName),
                        Bytes = bytes,
                        IsRestricted = true,
                        UserId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier))
                    };
                    if (newFile.Name.Length > 50)
                        return BadRequest("File name cannot be longer than 50 symbols");
                    if (newFile.Extension.Length > 50)
                        return BadRequest("File extension cannot be longer than 50 symbols");
                    var created = await _fileService.CreateFile(newFile);
                    return Ok(created);
                }
            }
        }


    }
}
