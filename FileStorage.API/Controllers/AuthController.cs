﻿using FileStorage.BLL.DTO;
using FileStorage.BLL.Exceptions;
using FileStorage.BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;

namespace FileStorage.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IEmailSender _emailSender;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public AuthController(IUserService userService,
            IEmailSender emailSender,
            IHttpContextAccessor httpContextAccessor)
        {
            this._userService = userService;
            this._emailSender = emailSender;
            this._httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Creates new user
        /// </summary>
        /// <param name="userSignUpDto"></param>
        /// <returns></returns>
        [HttpPost("Signup")]
        public async Task<IActionResult> SignUp(UserSignUpDto userSignUpDto)
        {
            try
            {
                await _userService.SignUp(userSignUpDto);
            }
            catch (FileStorageException ex)
            {
                return BadRequest(ex.Message);
            }
            return Created(string.Empty, string.Empty);
        }

        /// <summary>
        /// Returns jwt by credentials
        /// </summary>
        /// <param name="userLoginResource"></param>
        /// <returns></returns>
        [HttpPost("Signin")]
        public async Task<IActionResult> SignIn(UserLoginDto userLoginResource)
        {
            UserLoggedDto user = null;
            try
            {
                user = await _userService.SignIn(userLoginResource);
            }
            catch (FileStorageException ex)
            {
                switch (ex.ResponseCode)
                {
                    case 400:
                        return BadRequest("Email or password incorrect.");
                    case 404:
                        return NotFound("User not found");
                }
            }
            return Ok(user);
        }

        /// <summary>
        /// Sends email with password reset token
        /// </summary>
        /// <param name="forgotPasswordModel"></param>
        /// <returns></returns>
        [HttpPost("Forgot")]
        public async Task<IActionResult> ForgotPassword(ForgotPasswordDto forgotPasswordModel)
        {
            string token = "";
            try
            {
                token = await _userService.GetForgotPasswordToken(forgotPasswordModel);
            }
            catch
            {
                //always return ok to prevent email enumeration
                return Ok();
            }
            var callback = $"Use this token to reset password with the /Auth/ResetPassword api route: <br> {token}";
            var message = new MessageDto(new string[] { forgotPasswordModel.Email }, "Reset password token", callback);
            try
            {
                await _emailSender.SendEmailAsync(message);
            }
            catch (Exception ex)
            {
                return StatusCode(503, ex.Message);
            }

            return Ok();
        }

        /// <summary>
        /// Resets password 
        /// </summary>
        /// <param name="newPasswordDto"></param>
        /// <returns></returns>
        [HttpPost("Reset")]
        public async Task<IActionResult> ResetPassword(NewPasswordDto newPasswordDto)
        {
            try
            {
                await _userService.ResetPassword(newPasswordDto);
            }
            catch (Exception ex)
            {
                return StatusCode(503, ex.Message);
            }
            return Ok();
        }

    }
}
