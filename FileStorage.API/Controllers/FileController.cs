﻿using FileStorage.BLL.DTO;
using FileStorage.BLL.Exceptions;
using FileStorage.BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace FileStorage.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class FileController : ControllerBase
    {
        private readonly IFileService _fileService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        public FileController(IFileService fileService,
            IHttpContextAccessor httpContextAccessor)
        {
            this._fileService = fileService;
            this._httpContextAccessor = httpContextAccessor;
        }

        /// <summary>
        /// Returns files. If user is admin - returns all files. If not - only files uploaded by user
        /// </summary>
        /// <returns></returns>
        [HttpGet("")]
        public async Task<ActionResult<IEnumerable<FileDto>>> GetAllFiles()
        {
            IEnumerable<FileDto> files;

            if (User.IsInRole("Admin"))
                files = await _fileService.GetAllWithUserAsync();
            else
            {
                var userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
                files = await _fileService.GetFilesByUserIdAsync(userId);
            }

            return Ok(files);
        }

        /// <summary>
        /// Returns info about file by its id. 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<FileDto>> GetFileById(int id)
        {
            var target = await _fileService.GetFileByIdAsync(id);
            Guid userId;
            try
            {
                userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            }
            catch
            {
                userId = default;
            }

            if (target == null)
                return NotFound();

            if (userId != target.UserId && !User.IsInRole("Admin"))
                return BadRequest("You have no access to this file");

            return Ok(target);
        }

        /// <summary>
        /// Updates file name
        /// </summary>
        /// <param name="id"></param>
        /// <param name="file"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult<FileDto>> UpdateFile(int id, [FromBody] FileUpdateDto file)
        {
            Guid userId;
            try
            {
                userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            }
            catch
            {
                userId = default;
            }
            var fileToBeUpdate = await _fileService.GetFileByIdAsync(id);
            if (fileToBeUpdate == null)
                return NotFound();
            if (userId != fileToBeUpdate.UserId && !User.IsInRole("Admin"))
                return StatusCode(403, "You have no access to this file");

            await _fileService.UpdateFile(fileToBeUpdate, file);
            var updatedFile = await _fileService.GetFileByIdAsync(id);
            return Ok(updatedFile);
        }

        /// <summary>
        /// Deletes file
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFile(int id)
        {
            Guid userId;
            try
            {
                userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            }
            catch
            {
                userId = default;
            }
            var target = await _fileService.GetFileByIdAsync(id);
            if (target == null)
                return NotFound("There is no file with such id");
            if (userId != target.UserId && !User.IsInRole("Admin"))
                return StatusCode(403, "You have no access to this file");


            try
            {
                await _fileService.DeleteFile(id);
            }
            catch (FileStorageException ex)
            {
                return NotFound(ex.Message);
            }

            return NoContent();
        }

        [HttpPost("Share/{id}")]
        public async Task<IActionResult> SetFileAsShared(int id)
        {
            Guid userId;
            try
            {
                userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            }
            catch
            {
                userId = default;
            }
            var target = await _fileService.GetFileByIdAsync(id);
            if (target == null)
                return NotFound("There is no file with such id");
            if (userId != target.UserId && !User.IsInRole("Admin"))
                return StatusCode(403, "You have no access to this file");
            try
            {
                await _fileService.SetFileAsShared(id);
            }
            catch(FileStorageException ex)
            {
                return NotFound(ex.Message);
            }
            return Ok();
        }

        [HttpPost("Restrict/{id}")]
        public async Task<IActionResult> SetFileAsRestricted(int id)
        {
            Guid userId;
            try
            {
                userId = Guid.Parse(_httpContextAccessor.HttpContext.User.FindFirstValue(ClaimTypes.NameIdentifier));
            }
            catch
            {
                userId = default;
            }
            var target = await _fileService.GetFileByIdAsync(id);
            if (target == null)
                return NotFound("There is no file with such id");
            if (userId != target.UserId && !User.IsInRole("Admin"))
                return StatusCode(403, "You have no access to this file");
            try
            {
                await _fileService.SetFileAsRestricted(id);
            }
            catch (FileStorageException ex)
            {
                return NotFound(ex.Message);
            }
            return Ok();
        }
    }
}
