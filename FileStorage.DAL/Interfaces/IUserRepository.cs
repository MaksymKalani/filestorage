﻿using FileStorage.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorage.DAL.Interfaces
{
    public interface IUserRepository : IRepository<User>
    {
        Task<IEnumerable<User>> GetAllWithFilesAsync();
        Task<User> GetWithFilesByIdAsync(Guid id);
        Task<User> GetByIdAsync(Guid id);
    }
}
