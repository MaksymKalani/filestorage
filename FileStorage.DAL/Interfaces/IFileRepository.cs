﻿using FileStorage.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorage.DAL.Interfaces
{
    public interface IFileRepository : IRepository<FileData>
    {
        Task<IEnumerable<FileData>> GetAllWithUserAsync();
        Task<IEnumerable<FileData>> GetByName(string name, string extension);
        Task<FileData> GetWithUserByIdAsync(int id);
        Task<FileData> GetWithUserByHashAsync(string hash);
        Task<IEnumerable<FileData>> GetAllByUserIdAsync(Guid userId);
        Task<byte[]> GetBytesByIdAsync(int id);
        Task AddBytesAsync(byte[] bytes, string path);
        Task AddAsync(FileData file, byte[] bytes);
        Task UpdateAsync(FileData file);
        void Remove(int id);
    }
}
