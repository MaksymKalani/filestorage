﻿using System;
using System.Threading.Tasks;

namespace FileStorage.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IUserRepository Users { get; }
        IFileRepository Files { get; }
        Task<int> CommitAsync();
    }
}
