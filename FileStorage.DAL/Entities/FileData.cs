﻿namespace FileStorage.DAL.Entities
{
    public class FileData
    {

        public int FileId { get; set; }
        public string Name { get; set; }
        public string Extension { get; set; }
        public string BytesPath { get; set; }
        public string Hash { get; set; }
        public bool IsRestricted { get; set; }
        public long Size { get; set; }
        public User User { get; set; }
    }
}
