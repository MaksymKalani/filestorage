﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace FileStorage.DAL.Entities
{
    public class User : IdentityUser<Guid>
    {
        public User()
        {
            Files = new Collection<FileData>();
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public ICollection<FileData> Files { get; set; }
    }
}
