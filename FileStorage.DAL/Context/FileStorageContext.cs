﻿using FileStorage.DAL.Configurations;
using FileStorage.DAL.Entities;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;

namespace FileStorage.DAL.EF
{
    public class FileStorageContext : IdentityDbContext<User, Role, Guid>
    {
        public FileStorageContext(DbContextOptions<FileStorageContext> options) : base(options)
        {

        }
        public DbSet<FileData> Files { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder
                .ApplyConfiguration(new FileConfiguration());

            builder
                .ApplyConfiguration(new UserConfiguration());

        }
    }
}
