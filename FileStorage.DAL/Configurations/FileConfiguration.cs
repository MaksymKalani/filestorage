﻿using FileStorage.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FileStorage.DAL.Configurations
{
    public class FileConfiguration : IEntityTypeConfiguration<FileData>
    {
        public void Configure(EntityTypeBuilder<FileData> builder)
        {
            builder
                .HasKey(m => m.FileId);

            builder
                .Property(m => m.Name)
                .IsRequired()
                .HasMaxLength(50);

            builder
               .Property(m => m.FileId)
               .UseIdentityColumn();

            builder
                .Property(m => m.Extension)
                .IsRequired()
                .HasMaxLength(10);

            builder
                .ToTable("Files");
        }
    }
}
