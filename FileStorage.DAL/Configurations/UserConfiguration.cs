﻿using FileStorage.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FileStorage.DAL.Configurations
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder
                .HasKey(a => a.Id);

            builder
                .Property(p => p.Id).ValueGeneratedOnAdd();

            builder
                .Property(p => p.FirstName).HasMaxLength(15);

            builder
                .Property(p => p.LastName).HasMaxLength(15);

            builder
                .HasMany(f => f.Files)
                .WithOne(f => f.User);


            builder
                .ToTable("Users");
        }
    }
}
