﻿using FileStorage.DAL.EF;
using FileStorage.DAL.Interfaces;
using FileStorage.DAL.Repositories;
using System.Threading.Tasks;

namespace FileStorage.DAL
{
    public sealed class UnitOfWork : IUnitOfWork
    {
        private readonly FileStorageContext _context;
        private UserRepository _userRepository;
        private FileRepository _fileRepository;

        public UnitOfWork(FileStorageContext context)
        {
            this._context = context;
        }

        public IUserRepository Users => _userRepository = _userRepository ?? new UserRepository(_context);

        public IFileRepository Files => _fileRepository = _fileRepository ?? new FileRepository(_context);

        public async Task<int> CommitAsync()
        {
            return await _context.SaveChangesAsync();
        }

        public void Dispose()
        {
            _context.Dispose();
        }
    }
}
