﻿using FileStorage.DAL.EF;
using FileStorage.DAL.Entities;
using FileStorage.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorage.DAL.Repositories
{
    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(DbContext context) : base(context)
        {

        }

        /// <summary>
        /// Returns users with files
        /// </summary>
        /// <returns>IEnumerable<User></returns>
        public async Task<IEnumerable<User>> GetAllWithFilesAsync()
        {
            return await FileStorageContext.Users
                .Include(a => a.Files)
                .ToListAsync();
        }

        /// <summary>
        /// Returns user by id including files
        /// </summary>
        /// <param name="id">Id of user</param>
        /// <returns>User</returns>
        public Task<User> GetWithFilesByIdAsync(Guid id)
        {
            return FileStorageContext.Users
                .Include(a => a.Files)
                .SingleOrDefaultAsync(a => a.Id == id);
        }

        /// <summary>
        /// Returns user by id
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>User</returns>
        public async Task<User> GetByIdAsync(Guid id)
        {
            return await FileStorageContext.Users.FindAsync(id);
        }


        private FileStorageContext FileStorageContext
        {
            get { return Context as FileStorageContext; }
        }
    }
}
