﻿using FileStorage.DAL.EF;
using FileStorage.DAL.Entities;
using FileStorage.DAL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace FileStorage.DAL.Repositories
{
    public class FileRepository : Repository<FileData>, IFileRepository
    {
        /// <summary>
        /// Path to store files on server: "/home/FileStorage/"
        /// </summary>
        readonly static string homePath = (Environment.OSVersion.Platform == PlatformID.Unix ||
                      Environment.OSVersion.Platform == PlatformID.MacOSX)
                    ? Environment.GetEnvironmentVariable("HOME")
                    : Environment.ExpandEnvironmentVariables("%HOMEDRIVE%%HOMEPATH%");

        readonly string _path = Path.Combine(homePath, "FileStorage\\");
        public FileRepository(DbContext context) : base(context)
        {

        }

        /// <summary>
        /// Adds file to context and writes it to disc
        /// </summary>
        /// <param name="file"> Metadata about file </param>
        /// <param name="bytes"> File bytes </param>
        /// <returns></returns>
        public Task AddAsync(FileData file, byte[] bytes)
        {
            if (file == null)
                throw new ArgumentNullException($"File is null");
            if (bytes == null)
                throw new ArgumentNullException($"Bytes are null");

            return AddInternalAsync(file, bytes);
        }

        private async Task AddInternalAsync(FileData file, byte[] bytes)
        {
            var fullpath = _path + file.Name + file.Extension;
            await AddBytesAsync(bytes, fullpath);
            file.Hash = GetHashCode(fullpath);
            file.BytesPath = _path;
            file.Size = new FileInfo(fullpath).Length;
            await FileStorageContext.Files.AddAsync(file);
        }

        /// <summary>
        /// Removes file from db and deletes from disc
        /// </summary>
        /// <param name="entity"> File to delete </param>
        public void Remove(int id)
        {
            var target = FileStorageContext.Files.Find(id);
            if (target == null)
            {
                throw new ArgumentException($"File with this id does not exist");
            }
            Remove(target);
        }
        public new void Remove(FileData entity)
        {
            File.Delete(entity.BytesPath + entity.Name + entity.Extension);
            FileStorageContext.Files.Remove(entity);
        }

        /// <summary>
        /// Returns all files by certain user including user
        /// </summary>
        /// <param name="userId"> User Id</param>
        /// <returns> IEnumerable<FileData> </returns>
        public async Task<IEnumerable<FileData>> GetAllByUserIdAsync(Guid userId)
        {
            return await FileStorageContext.Files.Include(f => f.User).Where(f => f.User.Id == userId).ToListAsync();
        }

        /// <summary>
        /// Returns all files including user
        /// </summary>
        /// <returns> <IEnumerable<FileData> </returns>
        public async Task<IEnumerable<FileData>> GetAllWithUserAsync()
        {
            return await FileStorageContext.Files
                .Include(m => m.User)
                .ToListAsync();
        }

        /// <summary>
        /// Returns file by id including user
        /// </summary>
        /// <param name="id"> Id of file to return </param>
        /// <returns></returns>
        public async Task<FileData> GetWithUserByIdAsync(int id)
        {
            return await FileStorageContext.Files
                .Include(m => m.User)
                .SingleOrDefaultAsync(m => m.FileId == id);
        }



        /// <summary>
        /// Returns bytes by first hash chars
        /// </summary>
        /// <param name="hash"> First characters of hash </param>
        /// <returns> byte[] </returns>
        public async Task<byte[]> GetBytesByIdAsync(int id)
        {
            var targetInfo = await GetByIdAsync(id);
            if (targetInfo == null)
                throw new ArgumentException($"There is no file with such id");
            string path = targetInfo.BytesPath + targetInfo.Name + targetInfo.Extension;
            try
            {
                if (GetHashCode(path) != targetInfo.Hash)
                    throw new InvalidDataException("File on disc is corrupted");
            }
            catch (FileNotFoundException ex)
            {
                throw new InvalidDataException($"File on disc is not found: {ex.Message}");
            }

            byte[] bytes = await File.ReadAllBytesAsync(path);

            return bytes;
        }


        /// <summary>
        /// Writes array of bytes to file on disc by given path
        /// </summary>
        /// <param name="bytes">Byte array to write</param>
        /// <param name="path">Path on disc </param>
        /// <returns></returns>
        public async Task AddBytesAsync(byte[] bytes, string path)
        {
            using (FileStream SourceStream = File.Open(path, FileMode.Create))
            {
                SourceStream.Seek(0, SeekOrigin.End);
                await SourceStream.WriteAsync(bytes, 0, bytes.Length);
            }
        }

        /// <summary>
        /// Updates file data 
        /// </summary>
        /// <param name="file">New file data</param>
        /// <returns></returns>
        public Task UpdateAsync(FileData file)
        {
            if (file == null)
                throw new ArgumentNullException($"File is null");


            return UpdateInternalAsync(file);

        }

        private async Task UpdateInternalAsync(FileData file)
        {
            var target = await FileStorageContext.Files.FindAsync(file.FileId);
            if (target == null)
                throw new ArgumentException("Cannot update non existing file");

            File.Move(target.BytesPath + target.Name + target.Extension,
                target.BytesPath + file.Name + file.Extension);

            target.Name = file.Name;
            target.Extension = file.Extension;
        }



        /// <summary>
        /// Returns SHA256 hashcode of file on disc
        /// </summary>
        /// <param name="file"></param>
        /// <returns>string</returns>
        private string GetHashCode(string file)
        {
            using (FileStream stream = File.OpenRead(file))
            {
                SHA256Managed sha = new SHA256Managed();
                byte[] hash = sha.ComputeHash(stream);
                return BitConverter.ToString(hash).Replace("-", String.Empty);
            }
        }

        /// <summary>
        /// Returns FileData by first hash chars including user
        /// </summary>
        /// <param name="hash"></param>
        /// <returns>FileData</returns>
        public async Task<FileData> GetWithUserByHashAsync(string hash)
        {
            return await FileStorageContext.Files.Include(f => f.User).Where(f => f.Hash.StartsWith(hash)).SingleOrDefaultAsync();
        }

        public async Task<IEnumerable<FileData>> GetByName(string name, string extension)
        {
            var files = await FileStorageContext.Files.Where(f => f.Name.StartsWith(name) && f.Extension.Equals(extension)).ToListAsync();
            return files;
        }

        private FileStorageContext FileStorageContext
        {
            get { return Context as FileStorageContext; }
        }
    }
}
