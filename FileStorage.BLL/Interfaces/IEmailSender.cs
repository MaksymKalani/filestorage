﻿using FileStorage.BLL.DTO;
using System.Threading.Tasks;

namespace FileStorage.BLL.Interfaces
{
    public interface IEmailSender
    {
        Task SendEmailAsync(MessageDto message);
    }
}
