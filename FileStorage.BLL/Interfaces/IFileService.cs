﻿using FileStorage.BLL.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace FileStorage.BLL.Interfaces
{
    public interface IFileService
    {

        Task<IEnumerable<FileDto>> GetAllWithUserAsync();
        Task<FileDto> GetFileByIdAsync(int id);
        Task<FileDto> GetFileByHashAsync(string hash);
        Task<MemoryStream> GetFileStreamById(int id);
        Task<IEnumerable<FileDto>> GetFilesByUserIdAsync(Guid userId);
        Task<FileDto> CreateFile(FileDto newFile);
        Task UpdateFile(FileDto fileToBeUpdated, FileUpdateDto file);
        Task DeleteFile(int id);
        Task SetFileAsRestricted(int id);
        Task SetFileAsShared(int id);
        Task<IEnumerable<FileDto>> GetAllAsync();
    }
}
