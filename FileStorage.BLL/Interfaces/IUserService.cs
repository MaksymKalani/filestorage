﻿using FileStorage.BLL.DTO;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace FileStorage.BLL.Interfaces
{
    public interface IUserService
    {
        Task<IList<string>> GetRoles();
        Task SignUp(UserSignUpDto user);
        Task<UserLoggedDto> SignIn(UserLoginDto userLogin);
        Task CreateRole(string roleName);
        Task DeleteRole(string roleName);
        Task AddUserToRole(string userEmail, string roleName);
        Task<IEnumerable<UserDto>> GetAllAsync();
        Task<UserDto> GetUserById(Guid id);
        Task DeleteUser(Guid id);
        Task RemoveUserFromRole(string userEmail, string roleName);
        Task UpdateUser(UserUpdateDto updatedUser);
        Task<string> GetForgotPasswordToken(ForgotPasswordDto forgotPassword);
        Task ResetPassword(NewPasswordDto resetPasswordModel);
    }
}
