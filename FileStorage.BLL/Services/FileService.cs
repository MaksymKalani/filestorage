﻿using AutoMapper;
using FileStorage.BLL.DTO;
using FileStorage.BLL.Exceptions;
using FileStorage.BLL.Interfaces;
using FileStorage.DAL.Entities;
using FileStorage.DAL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace FileStorage.BLL.Services
{
    public class FileService : IFileService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        public FileService(IUnitOfWork unitOfWork, IMapper mapper)
        {
            this._unitOfWork = unitOfWork;
            this._mapper = mapper;
        }


        /// <summary>
        /// Creates file on disc and db
        /// </summary>
        /// <param name="newFile">File to create</param>
        /// <returns>FileDto</returns>
        public async Task<FileDto> CreateFile(FileDto newFile)
        {
            var userAdded = await _unitOfWork.Users.GetByIdAsync(newFile.UserId);
            var target = _mapper.Map<DAL.Entities.FileData>(newFile);
            target.User = userAdded;
            var filesSameName = await _unitOfWork.Files.GetByName(newFile.Name, newFile.Extension);
            if(filesSameName.Any())
            {
                target.Name = target.Name + $"({filesSameName.Count()})";
            }
            await _unitOfWork.Files.AddAsync(target, newFile.Bytes);
            await _unitOfWork.CommitAsync();
            return newFile;
        }


        /// <summary>
        /// Deletes file from db and disc
        /// </summary>
        /// <param name="id">Id of file to delete</param>
        /// <returns></returns>
        public async Task DeleteFile(int id)
        {
            _unitOfWork.Files.Remove(id);
            await _unitOfWork.CommitAsync();
        }


        /// <summary>
        /// Returns all files including user
        /// </summary>
        /// <returns>IEnumerable<FileDto></returns>
        public async Task<IEnumerable<FileDto>> GetAllWithUserAsync()
        {
            var files = await _unitOfWork.Files.GetAllWithUserAsync();
            var filesDtos = _mapper.Map<IEnumerable<FileDto>>(files);

            return filesDtos;
        }


        /// <summary>
        /// Returns all files
        /// </summary>
        /// <returns>IEnumerable<FileDto></returns>
        public async Task<IEnumerable<FileDto>> GetAllAsync()
        {
            var files = await _unitOfWork.Files.GetAllAsync();
            var filesDtos = _mapper.Map<IEnumerable<FileDto>>(files);

            return filesDtos;
        }

        /// <summary>
        /// Returns file by its id including user
        /// </summary>
        /// <param name="id">Id of file to return</param>
        /// <returns>FileDto</returns>
        public async Task<FileDto> GetFileByIdAsync(int id)
        {
            var file = await _unitOfWork.Files.GetWithUserByIdAsync(id);

            return _mapper.Map<FileDto>(file);
        }

        /// <summary>
        /// Returns file by its hash including user
        /// </summary>
        /// <param name="hash">Hash of file to return</param>
        /// <returns>FileDto</returns>
        public async Task<FileDto> GetFileByHashAsync(string hash)
        {
            var file = await _unitOfWork.Files.GetWithUserByHashAsync(hash);

            return _mapper.Map<FileDto>(file);
        }

        /// <summary>
        /// Returns MemoryStream of file by hash
        /// </summary>
        /// <param name="hash">Hash of file to return</param>
        /// <returns>MemoryStream</returns>
        public async Task<MemoryStream> GetFileStreamById(int id)
        {
            byte[] file;
            try
            {
                file = await _unitOfWork.Files.GetBytesByIdAsync(id);
            }
            catch(Exception ex)
            {
                throw new FileStorageException(ex.Message, 404);
            }

            return new MemoryStream(file);
        }

        /// <summary>
        /// Returns files by certain user including user
        /// </summary>
        /// <param name="userId">Id of user</param>
        /// <returns>IEnumerable<FileDto></returns>
        public async Task<IEnumerable<FileDto>> GetFilesByUserIdAsync(Guid userId)
        {
            var files = await _unitOfWork.Files.GetAllByUserIdAsync(userId);
            return _mapper.Map<IEnumerable<FileDto>>(files);
        }

        /// <summary>
        /// Updates file info 
        /// </summary>
        /// <param name="fileToBeUpdated">Old file info</param>
        /// <param name="file">New file info</param>
        /// <returns></returns>
        public async Task UpdateFile(FileDto fileToBeUpdated, FileUpdateDto file)
        {
            fileToBeUpdated.Name = file.Name;
            fileToBeUpdated.Extension = file.Extension;
            var filesSameName = await _unitOfWork.Files.GetByName(fileToBeUpdated.Name, fileToBeUpdated.Extension);
            if (filesSameName.Any())
            {
                fileToBeUpdated.Name = fileToBeUpdated.Name + $"({filesSameName.Count()})";
            }
            await _unitOfWork.Files.UpdateAsync(_mapper.Map<FileData>(fileToBeUpdated));
            await _unitOfWork.CommitAsync();
        }

        public async Task SetFileAsRestricted(int id)
        {
            var target = await _unitOfWork.Files.GetByIdAsync(id);
            if (target == null)
                throw new FileStorageException("File not found", 404);
            target.IsRestricted = true;
            await _unitOfWork.CommitAsync();
        }

        public async Task SetFileAsShared(int id)
        {
            var target = await _unitOfWork.Files.GetByIdAsync(id);
            if (target == null)
                throw new FileStorageException("File not found", 404);
            target.IsRestricted = false;
            await _unitOfWork.CommitAsync();
        }
    }
}
