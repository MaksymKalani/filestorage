﻿using AutoMapper;
using FileStorage.BLL.DTO;
using FileStorage.BLL.Exceptions;
using FileStorage.BLL.Interfaces;
using FileStorage.BLL.Settings;
using FileStorage.DAL.Entities;
using FileStorage.DAL.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FileStorage.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<Role> _roleManager;
        private readonly JwtSettings _jwtSettings;
        private readonly IMapper _mapper;


        public UserService(UserManager<User> userManager,
            IMapper mapper,
            RoleManager<Role> roleManager,
            IOptionsSnapshot<JwtSettings> jwtSettings,
            IUnitOfWork unitOfWork
            )
        {
            this._userManager = userManager;
            this._mapper = mapper;
            this._roleManager = roleManager;
            this._jwtSettings = jwtSettings.Value;
            this._unitOfWork = unitOfWork;

        }

        /// <summary>
        /// Adds user to role
        /// </summary>
        /// <param name="userEmail">Email of user to be added</param>
        /// <param name="roleName">Name of role</param>
        /// <returns></returns>
        public async Task AddUserToRole(string userEmail, string roleName)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == userEmail);

            var result = await _userManager.AddToRoleAsync(user, roleName);

            if (!result.Succeeded)
            {
                throw new FileStorageException(result.Errors.First().Description, 500);
            }
        }

        /// <summary>
        /// Removes user from role
        /// </summary>
        /// <param name="userEmail">Email of user to be added</param>
        /// <param name="roleName">Name of role</param>
        /// <returns></returns>
        public async Task RemoveUserFromRole(string userEmail, string roleName)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == userEmail);

            var result = await _userManager.RemoveFromRoleAsync(user, roleName);

            if (!result.Succeeded)
            {
                throw new FileStorageException(result.Errors.First().Description, 500);
            }
        }

        /// <summary>
        /// Returns list of strings with roles
        /// </summary>
        /// <returns></returns>
        public async Task<IList<string>> GetRoles()
        {
            var roles =  await _roleManager.Roles.Select(r => r.Name).ToListAsync();
            return roles;
        }

        /// <summary>
        /// Generated forgot password token for user
        /// </summary>
        /// <param name="forgotPassword"></param>
        /// <returns></returns>
        public async Task<string> GetForgotPasswordToken(ForgotPasswordDto forgotPassword)
        {
            var user = await _userManager.FindByEmailAsync(forgotPassword.Email.Normalize().ToUpperInvariant());
            if (user == null)
                throw new ArgumentException("There is no user with given email");
            return await _userManager.GeneratePasswordResetTokenAsync(user);
        }

        /// <summary>
        /// Resets password to new one
        /// </summary>
        /// <param name="resetPasswordModel"></param>
        /// <returns></returns>
        public async Task ResetPassword(NewPasswordDto resetPasswordModel)
        {
            var user = await _userManager.FindByEmailAsync(resetPasswordModel.Email.Normalize().ToUpperInvariant());
            if (user == null)
                throw new ArgumentException("There is no user with given email");
            var resetPassResult = await _userManager.ResetPasswordAsync(user, resetPasswordModel.Token, resetPasswordModel.Password);
            if (!resetPassResult.Succeeded)
                throw new InvalidOperationException(resetPassResult.Errors.First().ToString());
        }

        /// <summary>
        /// Creates new role
        /// </summary>
        /// <param name="roleName">Name of new role</param>
        /// <returns></returns>
        public async Task CreateRole(string roleName)
        {
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new FileStorageException("Role name should be provided", 400);
            }
            var newRole = new Role
            {
                Name = roleName
            };

            var roleResult = await _roleManager.CreateAsync(newRole);

            if (!roleResult.Succeeded)
            {
                throw new FileStorageException(roleResult.Errors.First().Description, 500);
            }

        }

        public async Task DeleteRole(string roleName)
        {
            if (string.IsNullOrWhiteSpace(roleName))
            {
                throw new FileStorageException("Role name should be provided", 400);
            }
            var target = await _roleManager.FindByNameAsync(roleName);
            if(target == null)
                throw new FileStorageException("There is no such role", 404);

            var roleResult = await _roleManager.DeleteAsync(target);

            if (!roleResult.Succeeded)
            {
                throw new FileStorageException(roleResult.Errors.First().Description, 500);
            }

        }

        /// <summary>
        /// Deletes user
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns></returns>
        public async Task DeleteUser(Guid id)
        {
            var target = await _unitOfWork.Users.GetByIdAsync(id);
            if (target == null)
                throw new ArgumentException("User with such id does not exist");
            var files = await _unitOfWork.Files.GetAllByUserIdAsync(id);
             _unitOfWork.Files.RemoveRange(files);
            _unitOfWork.Users.Remove(target);
        }

        /// <summary>
        /// Returns all users
        /// </summary>
        /// <returns>IEnumerable<UserDto></returns>
        public async Task<IEnumerable<UserDto>> GetAllAsync()
        {
            var users = await _unitOfWork.Users.GetAllWithFilesAsync();
            var usersDto = _mapper.Map<IEnumerable<UserDto>>(users);
            foreach (var user in usersDto)
            {
                user.Roles = (await _userManager.GetRolesAsync(await _unitOfWork.Users.GetByIdAsync(user.Id))).ToList();
            }
            return usersDto;
        }


        /// <summary>
        /// Returns user by id
        /// </summary>
        /// <param name="id">Id of user</param>
        /// <returns>UserDto</returns>
        public async Task<UserDto> GetUserById(Guid id)
        {
            var user = await _unitOfWork.Users.GetByIdAsync(id);
            var userDto = _mapper.Map<UserDto>(user);
            userDto.Roles = (await _userManager.GetRolesAsync(await _unitOfWork.Users.GetByIdAsync(user.Id))).ToList();
            return userDto;
        }


        public Task UpdateUser(UserUpdateDto updatedUser)
        {
            if (updatedUser == null)
                throw new ArgumentNullException($"Updated User is null");
            return UpdateUserInternal(updatedUser);

        }

        private async Task UpdateUserInternal(UserUpdateDto updatedUser)
        {
            var target = await _unitOfWork.Users.GetByIdAsync(updatedUser.Id);
            if (target == null)
                throw new FileStorageException("Cannot find user with given Id", 404);
            target.Email = updatedUser.Email;
            target.UserName = updatedUser.Email;
            target.FirstName = updatedUser.FirstName;
            target.LastName = updatedUser.LastName;
            await _unitOfWork.CommitAsync();
        }

        /// <summary>
        /// Returns JWT token if user login info is correct
        /// </summary>
        /// <param name="userLogin">User login info</param>
        /// <returns>string</returns>
        public async Task<UserLoggedDto> SignIn(UserLoginDto userLogin)
        {
            var user = _userManager.Users.SingleOrDefault(u => u.UserName == userLogin.Email);
            if (user is null)
            {
                throw new FileStorageException("User not found", 404);
            }

            var userSigninResult = await _userManager.CheckPasswordAsync(user, userLogin.Password);

            if (!userSigninResult)
            {
                throw new FileStorageException("Email or password incorrect.", 400);
            }
            var roles = await _userManager.GetRolesAsync(user);
            return new UserLoggedDto
            {
                Id = user.Id,
                Username = user.UserName,
                Token = GenerateJwt(user, roles),
                Roles = (await _userManager.GetRolesAsync(await _unitOfWork.Users.GetByIdAsync(user.Id))).ToList()
            };
        }

        /// <summary>
        /// Registers new user
        /// </summary>
        /// <param name="user">New user credentials</param>
        /// <returns></returns>
        public async Task SignUp(UserSignUpDto user)
        {
            var newUser = _mapper.Map<UserSignUpDto, User>(user);

            var userCreateResult = await _userManager.CreateAsync(newUser, user.Password);

            if (!userCreateResult.Succeeded)
            {
                throw new FileStorageException(userCreateResult.Errors.First().Description, 500);
            }
        }

        /// <summary>
        /// Generates JWT token
        /// </summary>
        /// <param name="user">User to generate for</param>
        /// <param name="roles">List of roles</param>
        /// <returns>string</returns>
        private string GenerateJwt(User user, IList<string> roles)
        {
            var claims = new List<Claim>
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString())
            };

            var roleClaims = roles.Select(r => new Claim(ClaimTypes.Role, r));
            claims.AddRange(roleClaims);

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_jwtSettings.Secret));
            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);
            var expires = DateTime.Now.AddDays(Convert.ToDouble(_jwtSettings.ExpirationInDays));

            var token = new JwtSecurityToken(
                issuer: _jwtSettings.Issuer,
                audience: _jwtSettings.Issuer,
                claims,
                expires: expires,
                signingCredentials: creds
            );

            return new JwtSecurityTokenHandler().WriteToken(token);
        }

    }
}
