﻿using System;
using System.Collections.Generic;

namespace FileStorage.BLL.DTO
{
    public class UserLoggedDto
    {
        public Guid Id { get; set; }
        public string Username { get; set; }
        public string Token { get; set; }
        public List<string> Roles { get; set; }
    }
}
