﻿using System.ComponentModel.DataAnnotations;

namespace FileStorage.BLL.DTO
{
    public class ResetPasswordDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
        [Required]
        public string Token { get; set; }
    }
}
