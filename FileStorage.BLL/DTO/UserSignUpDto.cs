﻿using System.ComponentModel.DataAnnotations;

namespace FileStorage.BLL.DTO
{
    public class UserSignUpDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "The First Name value cannot be null")]
        [MinLength(3)]
        [StringLength(15, ErrorMessage = "The First Name value cannot exceed 15 characters. ")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "No numbers allowed in First Name.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "The Last Name value cannot be null")]
        [MinLength(3)]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "No numbers allowed in Last Name.")]
        [StringLength(15, ErrorMessage = "The Last Name value cannot exceed 15 characters. ")]
        public string LastName { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Required]
        [Compare("Password", ErrorMessage = "Passwords do not match")]
        [DataType(DataType.Password)]
        public string ConfirmPassword { get; set; }
    }
}
