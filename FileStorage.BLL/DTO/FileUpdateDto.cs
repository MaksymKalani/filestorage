﻿using System.ComponentModel.DataAnnotations;

namespace FileStorage.BLL.DTO
{
    public class FileUpdateDto
    {
        [Required(ErrorMessage = "File name should not be empty")]
        [StringLength(15, ErrorMessage = "File name should not be longer than 15 characters")]
        [MinLength(3)]
        public string Name { get; set; }
        [Required(ErrorMessage = "File extension should not be empty")]
        [MinLength(3)]
        public string Extension { get; set; }
    }
}
