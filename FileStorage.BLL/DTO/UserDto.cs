﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FileStorage.BLL.DTO
{
    public class UserDto
    {
        public Guid Id { get; set; }
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        [JsonIgnore]
        public List<int> FileIds { get; set; }
        public List<string> Roles { get; set; }
    }
}
