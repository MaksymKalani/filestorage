﻿using System.ComponentModel.DataAnnotations;

namespace FileStorage.BLL.DTO
{
    public class ForgotPasswordDto
    {
        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
