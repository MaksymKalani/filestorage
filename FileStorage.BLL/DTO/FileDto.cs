﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace FileStorage.BLL.DTO
{
    public class FileDto
    {
        public int Id { get; set; }
        [Required(ErrorMessage = "File name should not be empty")]
        [StringLength(50, ErrorMessage = "File name should not be longer than 50 characters")]
        [MinLength(3)]
        public string Name { get; set; }
        [Required(ErrorMessage = "File extension should not be empty")]
        [MinLength(3)]
        [StringLength(10, ErrorMessage = "File extension should not be longer than 10 characters")]
        public string Extension { get; set; }
        [JsonIgnore]
        public byte[] Bytes { get; set; }
        public bool IsRestricted { get; set; }
        public long Size { get; set; }
        public string Hash { get; set; }
        public Guid UserId { get; set; }
    }
}
