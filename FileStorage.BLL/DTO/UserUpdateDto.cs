﻿using System;
using System.ComponentModel.DataAnnotations;

namespace FileStorage.BLL.DTO
{
    public class UserUpdateDto
    {
        [Required]
        public Guid Id { get; set; }

        [Required(ErrorMessage = "The First Name value cannot be null")]
        [MinLength(3)]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "No numbers allowed in First Name.")]
        [StringLength(15, ErrorMessage = "The First Name value cannot exceed 15 characters. ")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "The Last Name value cannot be null")]
        [MinLength(3)]
        [StringLength(15, ErrorMessage = "The Last Name value cannot exceed 15 characters. ")]
        [RegularExpression(@"^[a-zA-Z]+$", ErrorMessage = "No numbers allowed in Last Name.")]
        public string LastName { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }
    }
}
