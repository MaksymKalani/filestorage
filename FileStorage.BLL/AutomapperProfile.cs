﻿using AutoMapper;
using FileStorage.BLL.DTO;
using FileStorage.DAL.Entities;
using System.Linq;

namespace FileStorage.BLL
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<User, UserDto>()
                .ForMember(u => u.FileIds, c => c.MapFrom(u => u.Files.Select(x => x.FileId)))
                .ForMember(u => u.Email, c => c.MapFrom(u => u.Email))
                .ForMember(u => u.Email, c => c.MapFrom(u => u.UserName))
                .ForMember(u => u.Id, c => c.MapFrom(u => u.Id))
                .ReverseMap();

            CreateMap<UserDto, UserLoggedDto>()
                .ForMember(u => u.Roles, c => c.MapFrom(u => u.Roles))
                .ForMember(u => u.Username, c => c.MapFrom(u => u.Email))
                .ForMember(u => u.Id, c => c.MapFrom(u => u.Id))
                ;

            CreateMap<UserSignUpDto, User>()
                .ForMember(u => u.UserName, opt => opt.MapFrom(ur => ur.Email));

            CreateMap<FileData, FileDto>()

                .ForMember(u => u.UserId, c => c.MapFrom(u => u.User.Id))
                .ForMember(u => u.Name, c => c.MapFrom(u => u.Name))
                .ForMember(u => u.IsRestricted, c => c.MapFrom(u => u.IsRestricted))
                .ForMember(u => u.Hash, c => c.MapFrom(u => u.Hash))
                .ForMember(u => u.Size, c => c.MapFrom(u => u.Size))
                .ForMember(u => u.Extension, c => c.MapFrom(u => u.Extension))
                .ForMember(u => u.Id, c => c.MapFrom(u => u.FileId))
                .ReverseMap();

        }
    }
}
