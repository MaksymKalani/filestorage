﻿using System;
using System.Runtime.Serialization;

namespace FileStorage.BLL.Exceptions
{
    [Serializable]
    public class FileStorageException : Exception
    {
        public int ResponseCode { get; protected set; }
        public FileStorageException(string message, int code) : base(message)
        {
            ResponseCode = code;
        }

        protected FileStorageException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}
